#include <stdio.h>
#include <cs50.h>

#include <math.h> //round function

int main(void) {
	float a = get_float("Dollar Amount: ");

    int pennies = round(a * 100); //rounding

	printf("Amount in Pennies: %i\n", pennies);
	return 0;
}

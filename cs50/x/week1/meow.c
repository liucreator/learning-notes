#include <stdio.h>
#include <cs50.h>

// Prototype
void meow(int a);

int main(void) {
	int count = get_int("How many times? ");

	meow(count);
	return 0;
}

void meow(int a) {
	for (int i = 0; i < a; i++) {
		printf("uwu\n");
	}
}

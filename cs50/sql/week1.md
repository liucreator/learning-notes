# Relating

## Relational Databases

- One-to-One
- One-to-Many
- Many-to-Many

### Entity Relationship Diagrams (ER Diagrams)

Crow's foot notation

[![Crow's foot Notation](./crowsfoot-notation.png)](./crowsfoot-notation.png)

## Primary Key

In `longlist.db`, Book's ISBN is the primary key, they must be unique

## Foreign Key
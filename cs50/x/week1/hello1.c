#include <stdio.h>
#include <cs50.h>

int main(void) {
	// Get input
	string answer = get_string("What is your name? \n");
	
	printf("Hello, %s. This is a C program.\n", answer);
	return 0;
}

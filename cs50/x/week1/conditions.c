#include <stdio.h>
#include <cs50.h>

int main(void) {
	//get inputs
	int a = get_int("a: ");
	int b = get_int("b: ");

	if (a < b) {
		printf("a is less than b.\n");

	} else if (a > b) {
		printf("a is greater than b.\n");

	} else {
		printf("a is equal to b.\n");
	}
	return 0;
}

#include <stdio.h>
#include <cs50.h>

int main(void) {
	int w;
	do {
		w = get_int("Width: ");

	} while (w < 1); //if size less than 1, keep asking

	int h;
	do {
		h = get_int("Height: ");

	} while (h < 1);

	for (int i = 0; i < h; i++) {
		for (int j = 0; j < w; j++) {
			printf("#");
		}

		printf("\n");
	}
	return 0;
}
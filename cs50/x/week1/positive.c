#include <stdio.h>

int get_positive_int(void);

int main(void) {
	int a = get_positive_int();

	printf("%i\n",a);
	return 0;
}

int get_positive_int(void) {
	int b;
	do {
        	printf("Enter a positive Integer: ");
        	scanf("%i", &b);
	} while (b < 0);
	return b;
}

#include <stdio.h>
#include <cs50.h>

int main(void) {
	float a = get_float("a: ");

	float b = get_float("b: ");

	// format code for number of decimal places
	printf("Quotient: %.50f\n", a / b);
	return 0;

}

#include <stdio.h>
#include <cs50.h>

int main(void) {
	//get input from user
	int a = get_int("Number: ");

	if (a % 2 == 0) { //if remainder is 0 when divided by 2
		printf("Even\n");

	} else {
		printf("Odd\n");
	}
	return 0;
}

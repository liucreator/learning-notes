# Querying

## `SELECT`, `LIMIT`

```sql
SELECT "title", "author" FROM "longlist" 
LIMIT 5;
```

## `WHERE`

### Operators

- `=`
- `!=`
- `<>` not equal

Select all books not released in hardcover format:

```sql
SELECT "title", "format" FROM "longlist"
WHERE "format" <> 'hardcover';
```

### `NOT`

Select all books not released in hardcover format:

```sql
SELECT "title", "format" FROM "longlist"
WHERE NOT "format" = 'hardcover';
```

### Logical Keywords

- `AND`
- `OR`
- `()`

Select all books from 2022 or 2023

```sql
SELECT "title", "year" FROM "longlist"
WHERE "year" = 2022
OR "year" = 2023;
```

Select all hardcover books from 2022 and 2023

```sql
SELECT "title", "year" FROM "longlist"
WHERE ("year" = 2022 OR "year" = 2023)
AND "format" = 'hardcover';
```

### `NULL`

Select all books without a translator

```sql
SELECT "title", "translator" FROM "longlist"
WHERE "translator" IS NULL;
```

## `LIKE`

### Wildcards

- `%` match any characters
- `_` match a single character

Find all books with "love" in the title

```sql
SELECT "title" FROM "longlist"
WHERE "title" LIKE '%love%';
```

Find a Book whose title unsure how to spell

```sql
SELECT "title" FROM "longlist"
WHERE "title" LIKE 'P_re';
```

## Ranges

### Range Conditions

- `>`
- `<`
- `>=`
- `<=`

Find all books between 2019 and 2022

```sql
SELECT "title", "year" FROM "longlist"
WHERE "year" >= 2019 AND "year" <= 2022;
```

### `BETWEEN ... AND ...`

Find all books between 2019 and 2022

```sql
SELECT "title", "year " FROM "longlist"
WHERE "year" BETWEEN 2019 AND 2022;
```

Find all books with rating above 4.0 and has at least 1000 votes

```sql
SELECT "title", "rating", "votes" FROM "longlist"
WHERE "rating" > 4.0 AND "votes" > 1000;
```

## Sorting

### `ORDER BY`

`ORDER BY` *default to ascending*

- `ASC`
- `DESC`

Find top 10 rated books

```sql
SELECT "title", "rating" FROM "longlist"
ORDER BY "rating" DESC
LIMIT 10;
```

Find top 10 rated books and sorted by votes

```sql
SELECT "title", "rating", "votes" FROM "longlist"
ORDER BY "rating" DESC, "vote" DESC,
LIMIT 10;
```

Find top 10 rated books with more than 10000 votes

```sql
SELECT "title", "rating", "votes" FROM "longlist"
WHERE "votes" > 10000
ORDER BY "rating" DESC
LIMIT 10;
```

## Aggregate Function

- `COUNT()`
- `AVG()`
- `MIN()`
- `MAX()`
- `SUM()`

Find average rating of all books

```sql
SELECT AVG("rating") FROM "longlist";
```

- `ROUND(value, numberofdecimal)`
- `AS` rename column for display

Find rounded average rating of all books

```sql
SELECT ROUND(AVG("rating"), 2) AS "Average Rating" FROM "longlist";
```

Find the highest rating

```sql
SELECT MAX("rating") FROM "longlist";
```

Find the lowest rating

```sql
SELECT MIN("rating") FROM "longlist";
```

Find the total number of votes

```sql
SELECT SUM("votes") FROM "longlist";
```

Find total number of books(number of rows in dataset)

```sql
SELECT COUNT(*) FROM "longlist";
```

Find total number of translators

```sql
SELECT COUNT("translator") FROM "longlist";
```

`DISTINCT`

Find total number of unique publishers

```sql
SELECT COUNT(DISTINCT "publisher") FROM "longlist"
```

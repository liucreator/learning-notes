#include <stdio.h>
#include <cs50.h>

int main(void) {
	//constant point
	const int MINE = 5;

	int points = get_int("How many points you have?: ");

	if (points < MINE) {
		printf("You have less points than me.\n");

	} else if (points > MINE) {
		printf("You have more points than me.\n");

	} else {
		printf("You have the same number of points as me.\n");
	}
	return 0;
}
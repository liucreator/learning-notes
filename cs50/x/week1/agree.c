#include <stdio.h>
#include <cs50.h>

int main(void) {
	char a = get_char("Do you agree?(y/N) ");

	if (a == 'y' || a == 'Y') {
		printf("Agreed.\n");

	} else if (a == 'n' || a == 'N') {
		printf("Not Agreed.\n");

	}
	return 0;
}
